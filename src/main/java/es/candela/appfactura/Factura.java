package es.candela.appfactura;

import java.time.LocalDate;

/**
 * Una clase para representar Facturas.
 * @version 1.2, 23/02/21
 * @author JFdelaFuente
 */

final class Factura {

    private Factura() {
        super();
    }

    public static void imprimirFactura(Cliente cliente) {

        final int iva = 7;

        System.out.println("\nLicorería");
        System.out.println(generarNumFactura());
        System.out.println(generarFechaFactura());
        System.out.println(cliente.toString());
        System.out.println("Producto\tUnidad\tPrecio\tTotal");
        System.out.println(cliente.mostrarComprasCliente());
        float total = cliente.mostrarTotalCompra();
        double baseI =  total * iva;
        System.out.println("Base Imponible : " + baseI);
        System.out.println("IVA 21%        : " + (total - baseI));
        System.out.println("Total Fatura   : " + total);
    }

    public static String generarFechaFactura() {
        LocalDate ahora = LocalDate.now();
        String numFact = "Fecha Factura  :" + ahora;
        return numFact;
    }

    public static String generarNumFactura() {
        LocalDate ahora = LocalDate.now();
        String numFact = "Numero Factura :" + generarCodFactura() + "/" + ahora.getYear();
        return numFact;
    }

    public static String generarCodFactura() {
        String palabra = "";
        final int aleUno = 97;
        final int aleDos = 112;
        final int caracteres = 5;
        for (int i = 0; i < caracteres; i++) {
            int codigoAscii = (int) Math.floor(Math.random() * (aleDos - aleUno) + aleUno);
            palabra = palabra + (char) codigoAscii;
            palabra = palabra.toUpperCase();
        }
        return palabra;
    }
}
