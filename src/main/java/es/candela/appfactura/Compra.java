package es.candela.appfactura;

import java.time.LocalDate;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Una clase para representar Compras.
 * @version 1.2, 23/02/21
 * @author JFdelaFuente
 */

public class Compra {
    private LocalDate fecha;
    private String producto;
    private int cantidad;
    private float precio;

    static final Logger logger = LogManager.getLogger(Compra.class.getName());

    Compra(int year, int mes, int dia) {
        fecha = LocalDate.of(year, mes, dia);
    }

    Compra(String producto, int cantidad, float precio) {
        logger.debug("Inicializando compra.");
        this.producto = producto;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public float getPrecio() {
        return precio;
    }

    public String getProducto() {
        return producto;
    }

    public float getTotal() {
        return cantidad * precio;
    }

    public String mostrarCompra() {
        String salida = producto + "\t\t" + cantidad + "\t" + precio + "\t" + precio * cantidad;
        return salida;
    }

    public String toString() {
        return "\tInformacion de Compra:\n"
                + "Producto: " + producto + "\n"
                + "Cantidad: " + cantidad + "\n"
                + "Precio  : " + precio + "\n"
                + "Total   : " + cantidad * precio + "\n";
    }
}
