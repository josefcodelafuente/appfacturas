package es.candela.appfactura;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Una clase para representar Cliente.
 * @version 1.2, 23/02/21
 * @author JFdelaFuente
 */

public class Cliente {
    public static final int CERO = 0;
    public static final int UNO = 1;
    public static final int DOS = 2;
    public static final int TRES = 3;
    public static final int CUATRO = 4;

    private String dni;
    private String nombre;
    private String direccion;
    private String codigoPostal;
    private String provincia;
    private ArrayList<Compra> compras;

    static final Logger logger = LogManager.getLogger(Cliente.class.getName());

    public Cliente() {
        dni = "";
        nombre = "";
        direccion = "";
        codigoPostal = "";
        provincia = "";
        compras = new ArrayList<Compra>();
    }

    public Cliente(String n, String d, String dir, String codigo, String p) {
        logger.debug("Inicializando Cliente");
        dni = d;
        nombre = n;
        direccion = dir;
        codigoPostal = codigo;
        provincia = p;
        compras = new ArrayList<Compra>();
    }

    public String getCodigo_postal() {
        return codigoPostal;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getProvincia() {
        return provincia;
    }

    public String mostrarComprasCliente() {
        String salida = "";
        if (compras.size() > 0) {
            Iterator<Compra> itrClientes = compras.iterator();
            while (itrClientes.hasNext()) {
                Compra compra = itrClientes.next();
                salida += compra.mostrarCompra() + "\n";
            }
        }
        return salida;
    }

    public void mostrarCliente() {
        System.out.println(nombre + "\t" + dni + "\t" + direccion + "\t\t" + codigoPostal + "\t\t" + provincia);
    }

    public String toString() {
        return "\nInformacion de Facturación del Cliente:\n"
                + "   Nombre: " + nombre + "\n"
                + "   DNI: " + dni + "\n"
                + "   Direccion: " + direccion + "\n"
                + "   Codigo Postal: " + codigoPostal + "\n"
                + "   Provincia: " + provincia + "\n"
                + "\n";
    }

    public void addCompra(String producto, int cantidad, Float precio) {
        Compra compra = new Compra(producto, cantidad, precio);
        compras.add(compra);
        System.out.println(compra.toString());
    }

    public float mostrarTotalCompra() {
        float total = 0;
        if (compras.size() > 0) {
            Iterator<Compra> itrClientes = compras.iterator();
            while (itrClientes.hasNext()) {
                Compra compra = itrClientes.next();
                total += compra.getTotal();
            }
        }
        return total;
    }

    public void loadClienteFromFile(String br) {
        StringBuilder sb = new StringBuilder();
        sb.append(br);
        String[] datos = sb.toString().split("[|]");
        nombre = datos[CERO];
        dni = datos[UNO];
        direccion = datos[DOS];
        codigoPostal = datos[TRES];
        provincia = datos[CUATRO];
    }

    public void saveClienteToFile(BufferedWriter os) {
        try {
            os.write(nombre + "|" + dni + "|" + direccion + "|" + codigoPostal + "|" + provincia);
            os.newLine();
            os.flush();
        } catch (IOException e) {
            logger.debug(e.getMessage());
        }
    }

    public void loadCompraFromFile(String br) {
        StringBuilder sb = new StringBuilder();
        sb.append(br);
        String[] datos = sb.toString().split("[|]");
        Compra compra = new Compra(datos[UNO], Integer.parseInt(datos[DOS]), Float.parseFloat(datos[TRES]));
        compras.add(compra);
    }

    public void saveCompraToFile(BufferedWriter os) {
        try {
            if (compras.size() > 0) {
                Iterator<Compra> itrClientes = compras.iterator();
                while (itrClientes.hasNext()) {
                    Compra compra = itrClientes.next();
                    os.write(nombre + "|"
                            + compra.getProducto() + "|"
                            + compra.getCantidad() + "|"
                            + compra.getPrecio());
                    os.newLine();
                    os.flush();
                }
            }
        } catch (IOException e) {
            logger.debug(e.getMessage());
        }
    }
}
