package es.candela.appfactura;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.net.MalformedURLException;

/**
 * Unit test for simple App.
 */
class CompraTest {

    private static Logger LOGGER = null;
    private Compra compra = null;

    @BeforeAll
    public static void setLogger() throws MalformedURLException {
        System.setProperty("log4j.configurationFile", "log4j2-testConfig.xml");
        LOGGER = LogManager.getLogger();
    }

    @BeforeEach                                         
    public void setUp() {
        compra = new Compra("Pan", 2, (float)0.95);
    }
    
    /**
     * Rigorous Test.
     */
    @Test
    void test_compra_producto() {
        LOGGER.info("test compra producto Logged!!!");
        assertEquals("Pan", compra.getProducto());
    }

    @Test
    void test_compra_total() {
        LOGGER.info("test compra total precio producto Logged !!!");
        assertEquals(2*(float)0.95, compra.getTotal());
    }

    @Test
    void test_mostrar_compra() {
        LOGGER.info("test mostrar compra Logged !!!");
        assertEquals(compra.mostrarCompra(), compra.getProducto() 
                                            + "\t\t" + compra.getCantidad() 
                                            + "\t" + compra.getPrecio() 
                                            + "\t" + compra.getTotal());
    }
}