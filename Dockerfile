FROM openjdk:11-jre-slim

WORKDIR /home/app

# copy the pom and src code to the container
COPY target/appfactura-1.4-SNAPSHOT.jar appfacturas.jar
ADD target/lib/ lib/
COPY configuration.properties .
ADD data/ data/

# set the startup command to execute the jar
ENTRYPOINT ["java", "-jar", "appfacturas.jar"]
