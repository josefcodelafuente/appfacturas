pipeline {
    agent {
        docker {
            image 'maven:3.6.0'
            args '-v $HOME/.m2:/root/.m2'
        }
    }
    options {
        skipStagesAfterUnstable()
    }
    stages {
        stage('Build') {
            steps {
                echo 'Building'
                sh 'mvn -B -DskipTests clean package -PcodeCoverage -Dbuild.number=${BUILD_NUMBER}'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing'
                sh 'mvn test'
            }
        }
        stage('JaCoCo') {
            steps {
                echo 'Code Coverage'
                jacoco()
            }
        }

        stage('Sonar') {
            steps {
                echo 'Sonar Scanner'
                sh 'mvn verify sonar:sonar -Dsonar.projectKey=appfacturas \
                    -Dsonar.projectName=appFacturas \
                    -Dsonar.login=abd6d47b769275192d907abd1715a8b9128f5e2a \
                    -Dsonar.host.url=http://172.17.0.1:9000'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploy'
                sh 'mvn deploy -DskipTest -Dbuild.number=${BUILD_NUMBER} -s settings.xml'
            }
        }
    }
    post {
        always {
            echo 'JENKINS PIPELINE'
            archiveArtifacts artifacts: 'target/*.jar', fingerprint: true
            junit 'target/surefire-reports/*.xml'
        }
        success {
            echo 'JENKINS PIPELINE SUCCESSFUL'
        }
        failure {
            echo 'JENKINS PIPELINE FAILED'
        }
        unstable {
            echo 'JENKINS PIPELINE WAS MARKED AS UNSTABLE'
        }
        changed {
            echo 'JENKINS PIPELINE STATUS HAS CHANGED SINCE LAST EXECUTION'
        }
    }
}
